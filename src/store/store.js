import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

axios.defaults.baseURL = process.env.VUE_APP_API_URL;
axios.defaults.xhrFields = { withCredentials: true };
axios.defaults.cache = false;
axios.defaults.async = true;
axios.defaults.crossDomain = true;
axios.defaults.headers = {
    "Access-Control-Allow-Origin": "*",
    "accept": "application/json"
};

// axios.create({withCredentials: true})

const state = {

};

const mutations = {

};

const actions = {

};

const getters = {

};
export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})