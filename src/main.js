import Vue from 'vue';
import App from './App.vue';
import axios from 'axios';
import store from './store/store';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';
import router from './router';

Vue.config.productionTip = false;
Vue.prototype.$axios = axios;
Vue.use({store});



Vue.use(VueMaterial);


new Vue({
        store,
        router,
        render: h => h(App),
    }
).$mount('#app');
